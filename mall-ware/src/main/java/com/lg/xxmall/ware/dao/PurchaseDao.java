package com.lg.xxmall.ware.dao;

import com.lg.xxmall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:40:57
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
