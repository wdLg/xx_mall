package com.lg.xxmall.ware.dao;

import com.lg.xxmall.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:40:57
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
