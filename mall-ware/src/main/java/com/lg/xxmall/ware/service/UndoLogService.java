package com.lg.xxmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lg.common.utils.PageUtils;
import com.lg.xxmall.ware.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:40:58
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

