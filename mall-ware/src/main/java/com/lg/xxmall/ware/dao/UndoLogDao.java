package com.lg.xxmall.ware.dao;

import com.lg.xxmall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:40:58
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
