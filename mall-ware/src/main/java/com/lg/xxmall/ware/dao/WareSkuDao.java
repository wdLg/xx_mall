package com.lg.xxmall.ware.dao;

import com.lg.xxmall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:40:56
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}
