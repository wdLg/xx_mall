package com.lg.xxmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lg.common.utils.PageUtils;
import com.lg.xxmall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:36:05
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

