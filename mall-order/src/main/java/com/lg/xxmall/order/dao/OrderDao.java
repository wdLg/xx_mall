package com.lg.xxmall.order.dao;

import com.lg.xxmall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:36:05
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
