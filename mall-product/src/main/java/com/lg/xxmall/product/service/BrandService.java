package com.lg.xxmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lg.common.utils.PageUtils;
import com.lg.xxmall.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 17:23:25
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void updateDateil(BrandEntity brand);
}

