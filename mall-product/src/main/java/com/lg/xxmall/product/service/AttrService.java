package com.lg.xxmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lg.common.utils.PageUtils;
import com.lg.xxmall.product.entity.AttrEntity;
import com.lg.xxmall.product.vo.AttrRespVo;
import com.lg.xxmall.product.vo.AttrVo;
import com.lg.xxmall.product.vo.AttrgroupRelationVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 17:23:25
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String attrType);

    AttrRespVo getAttrInfo(Long attrId);


    void updateAttr(AttrRespVo attr);

    List<AttrEntity> getRelationAttr(Long attrgroupId);

    void deleteRelation(List<AttrgroupRelationVo> relationVos);

    PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId);
}

