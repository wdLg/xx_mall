package com.lg.xxmall.product.dao;

import com.lg.xxmall.product.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu属性值
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 17:23:24
 */
@Mapper
public interface ProductAttrValueDao extends BaseMapper<ProductAttrValueEntity> {
	
}
