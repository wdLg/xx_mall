package com.lg.xxmall.product.dao;

import com.lg.xxmall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 17:23:24
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
