package com.lg.xxmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lg.common.utils.PageUtils;
import com.lg.xxmall.product.entity.AttrAttrgroupRelationEntity;
import com.lg.xxmall.product.vo.AttrgroupRelationVo;

import java.util.List;
import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 17:23:25
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBatch(List<AttrgroupRelationVo> vos);
}

