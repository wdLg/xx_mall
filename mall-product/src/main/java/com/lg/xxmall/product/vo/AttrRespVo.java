package com.lg.xxmall.product.vo;

import lombok.Data;

/***
 *
 *
 * 描    述：
 *
 * 创 建 者：@author lg
 * 创建时间：2021/4/4下午2:48
 * 创建描述：
 *
 * 修 改 者：
 * 修改时间：
 * 修改描述：
 *
 * 审 核 者：
 * 审核时间：
 * 审核描述：
 *
 */
@Data
public class AttrRespVo extends AttrVo{

    /**
     *所属分类名字
     * */
    private String catelogName;

    /**
     *所属分组名字
     * */
    private String groupName;

    private Long[] catelogPath;
}
