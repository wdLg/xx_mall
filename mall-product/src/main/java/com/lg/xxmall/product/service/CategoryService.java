package com.lg.xxmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lg.common.utils.PageUtils;
import com.lg.xxmall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 17:23:25
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 按照树形结构返回
     * */
    List<CategoryEntity> listWithTree();


    void removeMenveByIds(List<Long> asList);


    /**
     * 找到catelogId 的完整路径
     * [夫/子/后代..]
     * */
    Long[] findCatelogPath(Long catelogId);

    void updateCascade(CategoryEntity category);
}

