package com.lg.xxmall.product.dao;

import com.lg.xxmall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 17:23:25
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
