package com.lg.xxmall.product.vo;

import lombok.Data;

/***
 *
 *
 * 描    述：
 *
 * 创 建 者：@author lg
 * 创建时间：2021/4/4下午7:23
 * 创建描述：
 *
 * 修 改 者：
 * 修改时间：
 * 修改描述：
 *
 * 审 核 者：
 * 审核时间：
 * 审核描述：
 *
 */
@Data
public class AttrgroupRelationVo {

    /**
     * 属性id
     */
    private Long attrId;
    /**
     * 属性分组id
     */
    private Long attrGroupId;
}
