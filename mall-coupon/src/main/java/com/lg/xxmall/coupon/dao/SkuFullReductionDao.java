package com.lg.xxmall.coupon.dao;

import com.lg.xxmall.coupon.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:18:46
 */
@Mapper
public interface SkuFullReductionDao extends BaseMapper<SkuFullReductionEntity> {
	
}
