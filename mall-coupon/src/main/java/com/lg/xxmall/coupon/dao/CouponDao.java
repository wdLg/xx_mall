package com.lg.xxmall.coupon.dao;

import com.lg.xxmall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:18:49
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
