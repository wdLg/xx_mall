package com.lg.xxmall.coupon.dao;

import com.lg.xxmall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:18:47
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
