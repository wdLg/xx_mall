package com.lg.xxmall.member.dao;

import com.lg.xxmall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:23:51
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
