package com.lg.xxmall.member.feign;

import com.lg.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/***
 *
 *
 * 描    述：
 *
 * 创 建 者：@author lg
 * 创建时间：2020/11/4下午4:30
 * 创建描述：
 *
 * 修 改 者：  
 * 修改时间： 
 * 修改描述： 
 *
 * 审 核 者：
 * 审核时间：
 * 审核描述：
 *
 */
@FeignClient("mall-coupon")
public interface CouponFeignService {


    @RequestMapping("/coupon/coupon/member/list")
    public R memberCoupons();
}
