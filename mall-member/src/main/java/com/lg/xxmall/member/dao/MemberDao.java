package com.lg.xxmall.member.dao;

import com.lg.xxmall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author lg
 * @email lg_0516@163.com
 * @date 2020-11-01 19:23:52
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
